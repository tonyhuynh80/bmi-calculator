import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BmiCalculatorComponent } from './bmi-calculator.component';
import { CalculateButtonModule } from '../shared-components/calculate-button/calculate-button.module';
import { ResultsComponentsModule } from '../shared-components/results-components/results-components.module';
import { SliderModule } from '../shared-components/slider-input/slider.module';

import { CalculatorParentComponent } from './calculator/calculator-parent.component';
import { ResultsParentComponent } from './results/results-parent.component';

@NgModule({
  declarations: [
    BmiCalculatorComponent,
    CalculatorParentComponent,
    ResultsParentComponent,
  ],
  imports: [
    CommonModule,
    CalculateButtonModule,
    ResultsComponentsModule,
    SliderModule,
  ],
  exports: [
    BmiCalculatorComponent,
  ]
})
export class BmiCalculatorModule { }
