import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator-parent',
  templateUrl: './calculator-parent.component.html',
  styleUrls: ['./calculator-parent.component.scss']
})
export class CalculatorParentComponent implements OnInit {
  title = 'BMI Calculator';
  min = 100
  max = 250
  min2 = 30
  max2 = 200
  
  constructor() { }

  ngOnInit() {
  }

}
