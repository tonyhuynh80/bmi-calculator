import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculateButtonComponent } from './calculate-button.component';


@NgModule({
  declarations: [
    CalculateButtonComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    CalculateButtonComponent,
  ]
})
export class CalculateButtonModule { }
