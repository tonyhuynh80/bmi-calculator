import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-results-indicator",
  templateUrl: "./results-indicator.component.html",
  styleUrls: ["./results-indicator.component.scss"]
})
export class ResultsIndicatorComponent implements OnInit {
  @Input() sliderVal: number = 19;
  @Input() min: number = 15;
  @Input() max: number = 35;
  range: number;
  // sliderPercent: string = "calc(0% - 2.25em)";
  constructor() {}

  ngOnInit() {
    this.range = this.max - this.min;
  }

  setSliderPercent(number: number): string {
    number = number - this.min;
    const percentage = (number / this.range) * 100;
    console.log(percentage);
    if (percentage > 99) {
      return `calc(99% - 2.25em)`;
    }
    if (percentage < 1) {
      return `calc(1% - 2.25em)`;
    }
    return `calc(${percentage}% - 2.25em)`;
  }

  setSliderColour(number: number) {
    number = number - this.min;
    const percentage = (number / this.range) * 100;
    if (percentage >= 75) {
      return { "background-color": "#d30404" };
    }
    if (percentage >= 50) {
      return { "background-color": "#ffc42d" };
    }
    if (percentage >= 25) {
      return { "background-color": "#45b443" };
    }
    return { "background-color": "#1b9cf9" };
  }
}
