import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsLegendComponent } from './results-legend.component';

describe('ResultsLegendComponent', () => {
  let component: ResultsLegendComponent;
  let fixture: ComponentFixture<ResultsLegendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsLegendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
