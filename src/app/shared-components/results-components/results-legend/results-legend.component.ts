import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-results-legend',
  templateUrl: './results-legend.component.html',
  styleUrls: ['./results-legend.component.scss']
})
export class ResultsLegendComponent implements OnInit {

  @Input() legend: [string, string, string, string] = ['Underweight', 'Healthy weight range', 'Overweight but not obese', 'Obese']

  constructor() { }

  ngOnInit() {
  }

}
