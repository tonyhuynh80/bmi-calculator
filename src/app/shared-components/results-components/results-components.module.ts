import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultsIndicatorComponent } from './results-indicator/results-indicator.component';
import { ResultsLegendComponent } from './results-legend/results-legend.component';
import { ResultsNoticeComponent } from './results-notice/results-notice.component';

@NgModule({
  declarations: [
    ResultsIndicatorComponent,
    ResultsLegendComponent,
    ResultsNoticeComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ResultsIndicatorComponent,
    ResultsLegendComponent,
    ResultsNoticeComponent,
  ]
})
export class ResultsComponentsModule { }