import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsNoticeComponent } from './results-notice.component';

describe('ResultsNoticeComponent', () => {
  let component: ResultsNoticeComponent;
  let fixture: ComponentFixture<ResultsNoticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsNoticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
