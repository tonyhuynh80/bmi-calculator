import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slider-parent',
  templateUrl: './slider-parent.component.html',
  styleUrls: ['./slider-parent.component.scss']
})
export class SliderParentComponent implements OnInit {
  @Input() min: number = 0;
  @Input() max: number = 100;
  @Input() startingVal: number = 0;
  @Input() title: string;
  @Input() unit: string;

  sliderValue: number;
  inputValue: number;

  constructor() { }

  ngOnInit() {
    this.sliderValue = this.startingVal;
    this.inputValue = this.startingVal;
  }

  getSliderValue(number) {
    this.sliderValue = number;
  }

  getInputValue(number) {
    this.inputValue = number;
  }
}