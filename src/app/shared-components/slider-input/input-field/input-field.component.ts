import { Component, OnInit, Input, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit {
  private inputField;
  
  @Input() min: number;
  @Input() max: number;
  @Input() inputValue: number = 0;
  @Input() unit: string;
  @Output() percentageOutput: EventEmitter<number> = new EventEmitter()

  range: number;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.inputField = this.el.nativeElement.querySelector('#inputField')
    this.range = this.max - this.min
    this.renderer.listen(this.inputField, 'input', () => {
      this.inputFieldInit()
    })
  }

  inputFieldInit() {
    const value = this.inputField.value;
    this.toPercentage(value)
  }

  toPercentage(number) {
    number = number - this.min;
    const percentage = number / this.range * 100;
    this.percentageOutput.emit(percentage);
  }
}