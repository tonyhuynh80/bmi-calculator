import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderComponent } from './slider/slider.component'
import { SliderParentComponent } from './slider-parent/slider-parent.component'
import { InputFieldComponent } from './input-field/input-field.component'

@NgModule({
  declarations: [
    SliderComponent,
    SliderParentComponent,
    InputFieldComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SliderComponent,
    SliderParentComponent,
    InputFieldComponent
  ]
})
export class SliderModule { }
