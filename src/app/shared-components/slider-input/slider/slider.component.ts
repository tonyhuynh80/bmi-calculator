import { Component, OnInit, ElementRef, Renderer2, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit, OnChanges {
  private slider;

  @Input() min: number;
  @Input() max: number;
  @Input() sliderVal: number;
  @Input() title: string;
  @Output() valueOutput: EventEmitter<number> = new EventEmitter()
  range: number;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {}


  ngOnInit() {
    this.slider = this.el.nativeElement.querySelector('#myRange')
    this.range = this.max - this.min
    this.slider.value = this.sliderVal
    this.sliderInit(this.slider.value)
    this.renderer.listen(this.slider, 'input', () => {
      this.sliderInit(this.slider.value)
      this.toValue(this.slider.value);
    })
  }

  ngOnChanges(changes) {
    if (this.slider) {
      this.sliderInit(changes.sliderVal.currentValue)
    }
  }

  sliderInit(value) {
    let color = 'linear-gradient(90deg, rgb(33,169,167)' + value + '%, rgba(0, 131, 144, 0.2)' + value + '%)';
    this.renderer.setStyle(this.slider, 'background', color);
  }

  toValue(number) {
    const numAsPercentage = number / 100;
    const output = Math.round(this.range * numAsPercentage);
    this.valueOutput.emit(output);
  }

}